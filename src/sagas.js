import { all } from 'redux-saga/effects'
import homeSaga from './conteiners/Home/sagas'

export default function* rootSaga () {
    yield all([
        homeSaga()
    ])
}