import React, { Component } from 'react'
import { Table, Statistic} from 'semantic-ui-react'

export default class TableComponent extends Component {
  render() {
    return (
      <Table>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell singleLine>Temperature</Table.HeaderCell>
                    <Table.HeaderCell>Pressure</Table.HeaderCell>
                    <Table.HeaderCell>Humidity</Table.HeaderCell>
                    <Table.HeaderCell>Max temperature</Table.HeaderCell>
                    <Table.HeaderCell>Min temperature</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                <Table.Row>
                    <Table.Cell>
                    <Statistic color='blue' size="tiny" inverted>
                        <Statistic.Value>{this.props.temperature}</Statistic.Value>
                    </Statistic>
                    </Table.Cell>
                    <Table.Cell singleLine>
                    <Statistic color='blue' size="tiny" inverted>
                        <Statistic.Value>{this.props.pressure}</Statistic.Value>
                    </Statistic>
                    </Table.Cell>
                    <Table.Cell>
                    <Statistic color='blue' size="tiny" inverted>
                        <Statistic.Value>{this.props.humidity}</Statistic.Value>
                    </Statistic>
                    </Table.Cell>
                    <Table.Cell>
                    <Statistic color='blue' size="tiny" inverted>
                        <Statistic.Value>{this.props.temp_max}</Statistic.Value>
                    </Statistic>
                    </Table.Cell>
                    <Table.Cell>
                    <Statistic color='blue' size="tiny" inverted>
                        <Statistic.Value>{this.props.temp_min}</Statistic.Value>
                    </Statistic>
                    </Table.Cell>
                </Table.Row>
            </Table.Body>
        </Table>
    )
  }
}
