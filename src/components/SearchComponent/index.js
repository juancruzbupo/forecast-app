import React, { Component } from 'react'
import { List, Label, Segment, Button } from 'semantic-ui-react'

export default class SearchComponent extends Component {
  render() {
    return (
      <Segment raised>
            <Label as='a' color='teal' tag>
                Last 5 search
            </Label>
            <List>
            { this.props.values.length > 0 ? (
                this.props.values.map((elemt, index) => {
                    return (<List.Item key={index} >
                                <List.Icon name='marker' />
                                <List.Content>
                                <List.Header as='a' onClick={() => this.props.handleChangeForListElemt(elemt)}>{elemt.name}</List.Header>
                                <List.Description>
                                    <div>
                                        Temp: {elemt.main.temp} - Pressure: {elemt.main.pressure} - Humidity: {elemt.main.humidity}
                                    </div>                                    
                                    <hr />
                                    <Button size="mini" icon='trash alternate' content='Borrar' onClick={() => this.props.handleDeleteSearch(index)}/>
                                </List.Description>
                                </List.Content>
                            </List.Item>)
                })
                ) : (
                    <span></span>
                )
            }
            </List>
        </Segment>
    )
  }
}
