import React, { Component } from 'react'
import { Container, Header, Icon } from 'semantic-ui-react'
import AppRoutes from './routes'
import './App.css'

import 'semantic-ui-css/semantic.min.css'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <Header as='h2' icon textAlign='center'>
          <Icon name='cloud' circular />
          <Header.Content>Forecast</Header.Content>
        </Header>
        <br />
        <Container textAlign='left'>
          <AppRoutes />  
        </Container>
      </div>
    )
  }
}

export default App
