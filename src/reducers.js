import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import homeReducer from './conteiners/Home/reducer'

const rootReducer = combineReducers({
    homeReducer,
    routing: routerReducer
});

export default rootReducer;
