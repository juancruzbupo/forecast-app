import {
    SEARCH_ACTION,
    SAVE_SEARCH_ACTION,
    DELETE_SEARCH_ACTION,
    SEARCH_GEOLOCATION_ACTION
} from './constants';

export const searchAction = (payload) => {
    return {
        type: SEARCH_ACTION,
        payload
    };
}

export const searchGeolocationAction = (payload) => {
    return {
        type: SEARCH_GEOLOCATION_ACTION,
        payload
    };
}

export const saveSearchAction = (payload) => {
    return {
        type: SAVE_SEARCH_ACTION,
        payload
    }
}

export const deleteSearchAction = (payload) => {
    return {
        type: DELETE_SEARCH_ACTION,
        payload
    }
}