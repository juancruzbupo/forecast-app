import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Form, Grid, Segment, Header, Button } from 'semantic-ui-react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import TableComponent from '../../components/TableComponent'
import SearchComponent from '../../components/SearchComponent'
import { searchAction, searchGeolocationAction, deleteSearchAction } from './accions'

export class componentName extends Component {
  state = {
    search: '',
    value: null,
    lastFive: []
  }
  
  static propTypes = {
    handleSearch: PropTypes.func,
    handleSearchGeolocalizacion: PropTypes.func,
    handleDeleteSearch: PropTypes.func,
    searchs: PropTypes.array
  }

  componentWillMount() {
    const {
      searchs
    } = this.props

    this.filter5shearch(searchs)
  }

  componentWillReceiveProps(nextProps) {
    const {
      searchs
    } = nextProps    
    console.log('====================================');
    console.log(searchs);
    console.log('====================================');
    
    if (searchs.length > 0){
      this.setState({
        value: searchs[0],
        search: searchs[0].name
      })
    }       
    this.filter5shearch(searchs)
  }

  handleChange = (e, { name, value }) => this.setState({ [name]: value })

  handleSubmit = () => {
    const {
      search
    } = this.state

    this.props.handleSearch({
      ciudad: search
    })
  }

  handleChangeForListElemt = (e) => {
    this.setState({
      value: e,
      search: e.name
    })
  }

  handleDeleteSearch = (index) => {
    this.props.handleDeleteSearch(index)
  }

  filter5shearch = (searchs) => {
    if (searchs.length < 6) {
      this.setState({
        lastFive: searchs
      })
    } else {
      const array = searchs.filter((elemt, index) => index < 5)
      this.setState({
        lastFive: array
      })
    }
  }

  handleClickGeolicalizacion = () => {
    let self = this;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (objPosition) {
        const lon = objPosition.coords.longitude;
        const lat = objPosition.coords.latitude;
        
        const payload = {
          lon,
          lat
        }

        self.props.handleSearchGeolocalizacion(payload);

      }, function (objPositionError) {
        switch (objPositionError.code) {
          case objPositionError.PERMISSION_DENIED:
            alert('No se ha permitido el acceso a la posición del usuario.');
            break;
          case objPositionError.POSITION_UNAVAILABLE:
            alert('No se ha podido acceder a la información de su posición.');
            break;
          case objPositionError.TIMEOUT:
            alert('El servicio ha tardado demasiado tiempo en responder.');
            break;
          default:
            alert('Error desconocido.');
        }
      }, {
        maximumAge: 75000,
        timeout: 15000
      });
    } else {
      alert('Su navegador no soporta la API de geolocalización.');
    }
  }

  render() {
    const {
      search,
      value,
      lastFive
    } = this.state

    let MyMapComponent = null

    if (value) {
      MyMapComponent = withScriptjs(withGoogleMap((props) =>
        <GoogleMap
          defaultZoom={8}
          defaultCenter = {
            {
              lat: value.coord.lat,
              lng: value.coord.lon
            }
          }
        >
          {
            props.isMarkerShown && < Marker position = {
              {
                lat: value.coord.lat,
                lng: value.coord.lon
              }
            }
            />
          }
        </GoogleMap>
      ))
    }

    return (
      <Grid>
        <Grid.Row>

          <Grid.Column width={11}>
            
          <Form onSubmit={this.handleSubmit}>
              <Form.Group>
                <Form.Input placeholder='search' name='search' width={13} value={search} onChange={this.handleChange} required />
                <Form.Button color='teal' content='Search' />
              </Form.Group>
            </Form>

            <Button color="blue" size="mini" icon='map pin' content='Buscar por geolocalizacion' onClick={this.handleClickGeolicalizacion} />
            
            { value ? (
                <Segment>
                  <Header as='h3'>{value.name}</Header>
                  <TableComponent 
                    temperature = {value.main.temp}
                    pressure =  {value.main.pressure}
                    humidity= {value.main.humidity}
                    temp_max = {value.main.temp_max}
                    temp_min = {value.main.temp_min}
                  />
                </Segment>
              ) : (
                <span />
              )
            }

            { value ?
              <div>
              <p className="text-uppercase font-weight-bold"> MAPA </p>
              <MyMapComponent
                isMarkerShown
                googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `100%`, width: `50%` }} />}
                containerElement={<div style={{ height: `400px` }} />}
                mapElement={<div style={{ height: `100%`, width: `100%`, marginBottom: `60px`}} />}
              />
              </div>
              : ''
            }

          </Grid.Column>

          <Grid.Column width={5}>
            
            <SearchComponent 
              values={lastFive} 
              handleChangeForListElemt={this.handleChangeForListElemt}
              handleDeleteSearch={this.handleDeleteSearch}
            />

          </Grid.Column>

        </Grid.Row>
        
      </Grid>
    )
  }
}

const mapStateToProps = (state) => ({
  searchs: state.homeReducer.searchs,
})

const mapDispatchToProps = (dispatch) => ({
  handleSearch: (payload) => dispatch(searchAction(payload)),
  handleSearchGeolocalizacion: (payload) => dispatch(searchGeolocationAction(payload)),
  handleDeleteSearch: (payload) => dispatch(deleteSearchAction(payload))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(componentName))
