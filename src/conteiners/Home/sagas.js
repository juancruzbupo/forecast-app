import { all, takeLatest, put, call } from 'redux-saga/effects';
import { SEARCH_ACTION, SEARCH_GEOLOCATION_ACTION } from './constants';
import { saveSearchAction } from './accions';

function* searchSaga(action) {
    try {        
        const ciudad = action.payload.ciudad

        const response = yield call(fetch, `http://api.openweathermap.org/data/2.5/weather?q=${ciudad}&appid=62de07b563bcffdbacd152cebefa7520`);

        if (response.ok) {
            // Bind the function to response before call it
            const jsonFunc = response.json.bind(response);
            // Convert the body stream to json data
            const jsonData = yield call(jsonFunc);

            yield put(saveSearchAction(jsonData))
        }   

    } catch (err) {
         console.log(err);
    }
}

function* searchGeolocationSaga(action) {
    try {
        const lat = action.payload.lat
        const lon = action.payload.lon

        const response = yield call(fetch, `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=62de07b563bcffdbacd152cebefa7520`);

        if (response.ok) {
            // Bind the function to response before call it
            const jsonFunc = response.json.bind(response);
            // Convert the body stream to json data
            const jsonData = yield call(jsonFunc);

            yield put(saveSearchAction(jsonData))
        }

    } catch (err) {
        console.log(err);
    }
}

function* watchSearch() {
    yield takeLatest(SEARCH_ACTION, searchSaga)
    yield takeLatest(SEARCH_GEOLOCATION_ACTION, searchGeolocationSaga)
}

export default function* HomeSaga() {
    yield all([
        watchSearch()
    ]);
}
