import {
    SAVE_SEARCH_ACTION,
    DELETE_SEARCH_ACTION
} from './constants';

const initialState = {
    searchs: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SAVE_SEARCH_ACTION:
            return Object.assign({}, state, {
                ...state,
                searchs: [action.payload, ...state.searchs]
            })
        case DELETE_SEARCH_ACTION: 
            return Object.assign({}, state, {
                ...state,
                searchs: [
                    ...state.searchs.slice(0, action.payload),
                    ...state.searchs.slice(action.payload + 1)
                ]
            })
        default:
            return state
    }
}

export default reducer;